<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ProfesionController@index');

Route::resource('personas', 'PersonaController');

Route::post('/profesiones', 'ProfesionController@postProfesiones');

Route::post('/municipios', 'MunicipioController@postMunicipios');
