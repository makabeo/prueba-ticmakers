<html>
@include('page.head')
<meta name="csrf-token" content="{{ csrf_token() }}">
<!-- Select2 -->
<link rel="stylesheet" href="/bower_components/select2/dist/css/select2.min.css">
<!-- bootstrap datepicker -->
<link rel="stylesheet" href="/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            @include('page.navbar')
            @include('page.sidebar')
            <!-- Content Wrapper. Contains page content -->
                <div class="content-wrapper">
                    <!-- Content Header (Page header) -->
                    <section class="content-header">
                        <h1>
                            Servicio de Personas
                        </h1>
                        <ol class="breadcrumb">
                            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                            <li class="active">Profesiones</li>
                        </ol>
                    </section>

                    <!-- Main content -->
                    <section class="content">

                        @if ( session()->has('message') )
                            <div class="alert alert-success alert-dismissable">{{ session()->get('message') }}</div>
                        @endif

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <!-- general form elements -->
                        <div class="box box-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Crear Persona</h3>
                            </div>
                            <!-- /.box-header -->
                            <!-- form start -->
                            <form role="form" method="POST" action="{{url('personas')}}">
                                {{csrf_field()}}
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="nombre">Nombre de la Persona</label>
                                        <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Ingrese Nombre" minlength="3" maxlength="50" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="email">Correo</label>
                                        <input type="email" class="form-control" id="email" name="email" placeholder="Ingrese Correo" maxlength="50" required>
                                    </div>

                                    <!-- select -->
                                    <div class="form-group">
                                        <label>Profesion</label>
                                        <select id="profesion" name="profesion" class="form-control">
                                        </select>
                                    </div>

                                    <!-- select -->
                                    <div class="form-group">
                                        <label>Municipio</label>
                                        <select id="municipio" name="municipio" class="form-control">
                                        </select>
                                    </div>

                                    <!-- Date -->
                                    <div class="form-group">
                                        <label>Date:</label>

                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control pull-right" id="fechaNacimiento" name="fechaNacimiento">
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                    <!-- /.form group -->

                                </div>
                                <!-- /.box-body -->



                                <div class="box-footer">
                                    <button type="submit" class="btn btn-primary">Crear</button>
                                </div>
                            </form>
                        </div>
                        <!-- /.box -->


                    </section>
                    <!-- /.content -->
                </div>
                <!-- /.content-wrapper -->
            @include('page.footer')
        </div>
        @include('page.scripts')
        <!-- Select2 -->
        <script src="/bower_components/select2/dist/js/select2.full.min.js"></script>
        <!-- bootstrap datepicker -->
        <script src="/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
        <script>

            $.ajax({
                url: '/profesiones',
                type: 'post',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                dataType: 'json',
                success: function (data) {
                    $.each(data,function (index, valor) {
                        $('#profesion').append('<option value="'+valor.id+'">'+ valor.nombre_profesion+'</option>');
                    })
                }
            });

            $.ajax({
                url: '/municipios',
                type: 'post',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                dataType: 'json',
                success: function (data) {
                    $.each(data,function (index, valor) {
                        $('#municipio').append('<option value="'+valor.id+'">'+ valor.nombre_municipio+'</option>');
                    })
                }
            });

            $(function () {
                //Date picker
                $('#fechaNacimiento').datepicker({
                    format: 'yyyy-mm-dd',
                    autoclose: true
                })
            })
        </script>
    </body>
</html>
