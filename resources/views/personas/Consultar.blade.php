<html>
@include('page.head')
<link rel="stylesheet" href="/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            @include('page.navbar')
            @include('page.sidebar')
            <!-- Content Wrapper. Contains page content -->
                <div class="content-wrapper">
                    <!-- Content Header (Page header) -->
                    <section class="content-header">
                        <h1>
                            Servicio de Personas
                        </h1>
                        <ol class="breadcrumb">
                            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                            <li class="active">Profesiones</li>
                        </ol>
                    </section>

                    <!-- Main content -->
                    <section class="content">

                        @if ( session()->has('message') )
                            <div class="alert alert-success alert-dismissable">{{ session()->get('message') }}</div>
                        @endif

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">Personas</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <table id="profesiones" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Nombre Persona</th>
                                        <th>Profesion</th>
                                        <th>Municipio</th>
                                        <th>Email</th>
                                        <th>Acciones</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($personas as $persona)
                                        <tr>
                                            <td>{{$persona->nombre_persona}}</td>
                                            <td>{{$persona->profesion->nombre_profesion}}</td>
                                            <td>{{$persona->municipio->nombre_municipio}}</td>
                                            <td>{{$persona->email}}</td>
                                            <td>
                                                <div class="btn-group">
                                                    <a type="button" href="personas/{{$persona->id}}/edit" class="btn btn-default fa fa-fw fa-pencil"></a>

                                                    <form style="float: left" method="POST" action="{{url('personas/'.$persona->id)}}">
                                                        <input type="hidden" name="_method" value="DELETE">
                                                        {{csrf_field()}}
                                                        <button class="btn btn-danger fa fa-fw fa-trash"></button>
                                                    </form>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th>Nombre Persona</th>
                                        <th>Profesion</th>
                                        <th>Municipio</th>
                                        <th>Email</th>
                                        <th>Acciones</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <!-- /.box-body -->
                        </div>


                    </section>
                    <!-- /.content -->
                </div>
                <!-- /.content-wrapper -->
            @include('page.footer')
        </div>
        @include('page.scripts')
        <!-- DataTables -->
        <script src="/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <script>
            $(function () {
                $('#profesiones').DataTable({
                    "language": {
                        "sProcessing":     "Procesando...",
                        "sLengthMenu":     "Mostrar _MENU_ registros",
                        "sZeroRecords":    "No se encontraron resultados",
                        "sEmptyTable":     "Ningún dato disponible en esta tabla",
                        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                        "sInfoPostFix":    "",
                        "sSearch":         "Buscar:",
                        "sUrl":            "",
                        "sInfoThousands":  ",",
                        "sLoadingRecords": "Cargando...",
                        "oPaginate": {
                            "sFirst":    "Primero",
                            "sLast":     "Último",
                            "sNext":     "Siguiente",
                            "sPrevious": "Anterior"
                        },
                        "oAria": {
                            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                        }
                    }
                })
            })
        </script>
    </body>
</html>
