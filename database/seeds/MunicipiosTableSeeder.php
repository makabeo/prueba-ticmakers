<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class MunicipiosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('es_ES');
        for ($i=0; $i < 50; $i++)
        {
            \DB::table('municipios')->insert(array
            (
                'nombre_municipio' => $faker->city,
                'created_at' => date('Y-m-d H:m:s'),
                'updated_at' => date('Y-m-d H:m:s')
            ));
        }
    }
}
