<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class ProfesionesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for ($i=0; $i < 50; $i++)
        {
            \DB::table('profesiones')->insert(array
            (
                'nombre_profesion' => $faker->jobTitle,
                'created_at' => date('Y-m-d H:m:s'),
                'updated_at' => date('Y-m-d H:m:s')
            ));
        }
    }
}
