<?php

namespace App\Http\Controllers;

use App\Persona;
use App\Municipio;
use App\Profesion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PersonaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $personas = Persona::all();
        return view('personas.Consultar',['personas' => $personas]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('personas.Crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $municipioID = $request->input('municipio');
        $municipio = Municipio::find($municipioID);
        $profesionID = $request->input('profesion');
        $profesion = Profesion::find($profesionID);

        $validator = Validator::make($request->all(), [
            'nombre' => 'required|regex:/^[\pL\s\-]+$/u|max:30',
            'email' => 'required|string|max:255',
        ]);
        if ($validator->fails()) {
            return redirect('personas/create')
                ->withErrors($validator)
                ->withInput();
        }
        else
        {
            $informacionPersona =array(
                'nombre_persona' => $request->input('nombre'),
                'email' => $request->input('email'),
                'fecha_nacimiento' => $request->input('fechaNacimiento'),
                'Municipio_id' => $request->input('municipio'),
                'Profesion_id' => $request->input('profesion'),
            );
            $persona = Persona::create($informacionPersona);
            $persona->municipio()->associate($municipio);
            $persona->profesion()->associate($profesion);
            return redirect('personas/create')->with('message', 'La persona ha sido creada correctamente');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Persona  $persona
     * @return \Illuminate\Http\Response
     */
    public function show(Persona $persona)
    {
        return view('personas.Mostrar',['persona' => $persona]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Persona  $persona
     * @return \Illuminate\Http\Response
     */
    public function edit(Persona $persona)
    {
        return view('personas.Editar',['persona' => $persona]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Persona  $persona
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Persona $persona)
    {
        $validator = Validator::make($request->all(), [
            'nombre' => 'required|regex:/^[\pL\s\-]+$/u|max:30',
            'email' => 'required|string|max:255',
        ]);
        if ($validator->fails()) {
            return redirect('persona')
                ->withErrors($validator)
                ->withInput();
        }
        else
        {
            if ($request->input('nombre') != $persona->nombre_persona)
            {
                $persona->nombre_persona = $request->input('nombre');
            }
            if ($request->input('email') != $persona->email)
            {
                $persona->email = $request->input('email');
            }
            if ($request->input('fechaNacimiento') != $persona->fecha_nacimiento)
            {
                $persona->fecha_nacimiento = $request->input('fechaNacimiento');
            }
            if ($request->input('municipio') != $persona->municipio->id)
            {
                $municipio = Municipio::find($request->input('municipio'));
                $persona->municipio()->associate($municipio);
            }
            if ($request->input('profesion') != $persona->profesion->id)
            {
                $profesion = Municipio::find($request->input('profesion'));
                $persona->profesion()->associate($profesion);
            }
            $persona->save();
            return redirect('personas')->with('message', 'La persona ha sido modificada correctamente');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Persona  $persona
     * @return \Illuminate\Http\Response
     */
    public function destroy(Persona $persona)
    {
        $persona->delete();
        return redirect('personas')->with('message', 'La persona ha sido eliminada correctamente');
    }
}
