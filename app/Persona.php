<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    protected $table = 'personas';

    protected $fillable = ['nombre_persona','email','fecha_nacimiento','Municipio_id','Profesion_id'];

    public function profesion()
    {
        return $this->belongsTo('App\Profesion', 'Profesion_id');
    }

    public function municipio()
    {
        return $this->belongsTo('App\Municipio', 'Municipio_id');
    }
}
