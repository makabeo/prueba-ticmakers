<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profesion extends Model
{
    protected $table = 'profesiones';

    protected $fillable = ['nombre_profesion'];

    public function personas()
    {
        return $this->hasMany('App\Persona', 'Profesion_id');
    }
}
