<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Municipio extends Model
{
    protected $table = 'municipios';

    protected $fillable = ['nombre_municipio'];

    public function personas()
    {
        return $this->hasMany('App\Persona', 'Municipio_id');
    }
}
